package com.muhardin.endy.belajar.oauth.resourceserver.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muhardin.endy.belajar.oauth.resourceserver.dao.PenggunaDao;
import com.muhardin.endy.belajar.oauth.resourceserver.dao.TransaksiDao;
import com.muhardin.endy.belajar.oauth.resourceserver.entity.Pengguna;
import com.muhardin.endy.belajar.oauth.resourceserver.entity.Transaksi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController @Slf4j
public class TransaksiController {
    @Autowired private TransaksiDao transaksiDao;
    @Autowired private PenggunaDao penggunaDao;
    @Autowired private ObjectMapper objectMapper;

    @GetMapping("/api/transaksi/")
    public Iterable<Transaksi> dataTransaksi(Authentication currentUser) throws JsonProcessingException {
        log.info("Current user : {}", objectMapper.writeValueAsString(currentUser));
        String username = ((OAuth2AuthenticatedPrincipal) currentUser.getPrincipal())
                .getAttribute("user_name");
        log.info("Username : {}", username);
        Pengguna pengguna = penggunaDao.findByUsername(username);
        return transaksiDao.findByPengguna(pengguna);
    }
}
