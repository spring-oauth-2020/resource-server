package com.muhardin.endy.belajar.oauth.resourceserver.dao;

import com.muhardin.endy.belajar.oauth.resourceserver.entity.Pengguna;
import com.muhardin.endy.belajar.oauth.resourceserver.entity.Transaksi;
import org.springframework.data.repository.CrudRepository;

public interface TransaksiDao extends CrudRepository<Transaksi, String> {
    Iterable<Transaksi> findByPengguna(Pengguna pengguna);
}
