package com.muhardin.endy.belajar.oauth.resourceserver.dao;

import com.muhardin.endy.belajar.oauth.resourceserver.entity.Pengguna;
import org.springframework.data.repository.CrudRepository;

public interface PenggunaDao extends CrudRepository<Pengguna, String> {
    Pengguna findByUsername(Object sub);
}
