# Cara Menggunakan #

1. Nyalakan dulu `authorization-server`. Ada 2 pilihan :
 
    * [Opaque Token](https://gitlab.com/training-microservices-2019-01/authserver/-/tree/03ca72593084394295becffa7375a22570981737) 
    * [JWT](https://gitlab.com/spring-oauth-2020/authorization-server)
    * [KeyCloak](https://www.keycloak.org/)

2. Login ke `authorization-server` di [http://localhost:8080/oauth/authorize?client_id=clientwebbased&grant_type=authorization_code&response_type=code](http://localhost:8080/oauth/authorize?client_id=clientwebbased&grant_type=authorization_code&response_type=code)
3. Dapatkan `code` dalam redirect URI seperti ini

    ```
    http://localhost:10000/handle-oauth-callback?code=mTMBMx
    ```

4. Tukarkan dengan `access_token`. Berikut caranya dengan menggunakan `curl`

    ```
    curl --location --request POST 'http://localhost:8080/oauth/token ' \
    --user 'clientwebbased:abcd' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data-urlencode 'client_id=clientwebbased' \
    --data-urlencode 'grant_type=authorization_code' \
    --data-urlencode 'code=mTMBMx'
    ```
   
   Hasilnya seperti ini:
   
   ```json
   {
     "access_token":"f6e8a324-f0d3-40e1-a8ea-87adeea89166",
     "token_type":"bearer",
     "refresh_token":"e842246a-7c8d-4b47-99c2-9cedaf708f37",
     "expires_in":43199,
     "scope":"entri_data review_transaksi approve_transaksi"
   }
   ```

5. Gunakan `access_token` untuk request

    ```
    curl --location --request GET 'http://localhost:20000/api/transaksi/' \
    --header 'Authorization: Bearer 4ad35bbe-ab80-4b84-b6b0-8c6bf1aa4f79'
    ```